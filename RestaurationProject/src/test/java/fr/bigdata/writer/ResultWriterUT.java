package fr.bigdata.writer;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ResultWriterUT {
    SparkSession sparkSession = SparkSession.builder()
            .master("local[2]")
            .appName("test-writer")
            .getOrCreate();
    String outputFolderPathStr = "";

    @Before
    public void setup() {
        cleanup();
    }

    @Test
    public void testWriter() {
        ResultWriter resultWriter = new ResultWriter(outputFolderPathStr);
        Dataset<String> lines = sparkSession.range(0,10,2,2)
                .map((MapFunction<Long, String>) l -> String.format("line %d",l), Encoders.STRING());
        //resultWriter.accept(lines);

        Dataset<Row> actual = sparkSession.read().text("");

        String[] expected = new String[]{"line 0", "line 2", "line 4", "line 6", "line 8"};

        assertThat(actual.collectAsList().contains(expected));
    }

    @After
    public void tearDown() {
        cleanup();
    }

    public void cleanup() {
        try {
            Path outputPath = Paths.get(outputFolderPathStr);
            Files.list(outputPath).forEach(path -> {
                try{Files.deleteIfExists(path);}catch (IOException ioException) {
                    log.warn("could not delete path={} due to ...", path, ioException);
                }
            });
            Files.deleteIfExists(outputPath);
        }catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
