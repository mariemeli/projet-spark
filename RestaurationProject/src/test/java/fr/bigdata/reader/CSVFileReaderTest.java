package fr.bigdata.reader;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CSVFileReaderTest {
    @Test
    public void testCSVFileReader() {
        CSVFileReader csvFileReader = new CSVFileReader("src/test/resources/test.csv");
        Dataset<Row> data = csvFileReader.get();

        long actual = data.count();
        int expected = 1;

        assertThat(actual).isNotZero();
        assertThat(actual).isEqualTo(expected);
    }
}
