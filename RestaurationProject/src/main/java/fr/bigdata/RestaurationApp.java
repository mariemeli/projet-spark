package fr.bigdata;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import fr.bigdata.bean.Restaurant;
import fr.bigdata.functions.ConvertLinesToRestaurant;
import fr.bigdata.functions.GroupByServiceFunc;
import fr.bigdata.reader.CSVFileReader;
import fr.bigdata.writer.ResultWriter;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class RestaurationApp {
    public static void main(String[] args) {
        Config config = ConfigFactory.load();
        String inputPathStr = config.getString("Restauration.path.input");
        String outputPathStr = config.getString("Restauration.path.output");

        CSVFileReader csvFileReader = new CSVFileReader(inputPathStr);
        ConvertLinesToRestaurant convertLinesToRestaurant = new ConvertLinesToRestaurant();
        GroupByServiceFunc groupByServiceFunc = new GroupByServiceFunc();
        ResultWriter resultWriter = new ResultWriter(outputPathStr + "/groupByService");

        Dataset<Row> restaurantsRow = csvFileReader.get();
        Dataset<Restaurant> restaurants = convertLinesToRestaurant.apply(restaurantsRow);
        Dataset<Row> groupByService = groupByServiceFunc.apply(restaurantsRow);

        //groupByService.show();
        resultWriter.accept(groupByService);

    }
}
