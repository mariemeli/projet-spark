package fr.bigdata.writer;

import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class ResultWriter implements Consumer<Dataset<Row>> {
    private final String outputPathStr;

    @Override
    public void accept(Dataset<Row> rows) {
        rows.coalesce(1).write().mode(SaveMode.Overwrite).csv(outputPathStr);
                /*.format("csv")
                .option("sep", ";")
                .option("header", "true")
                .save(outputPathStr);*/

    }
}
