package fr.bigdata.functions;

import fr.bigdata.bean.Restaurant;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Row;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.function.Function;

@Slf4j
public class LineToRestaurant implements Function<Row, Restaurant>, Serializable {
    @Override
    public Restaurant apply(Row line) {
        if(line == null) {
            return null;
        }

        Restaurant restaurant = null;

        try {
            restaurant = Restaurant.builder()
                    .date((line.get(0) != null) ? new SimpleDateFormat("dd/MM/yyyy").parse(line.get(0).toString()): null)
                    .service((line.get(1) != null) ? line.get(1).toString(): null)
                    .composante((line.get(2) != null) ? line.get(2).toString(): null)
                    .plat((line.get(3) != null) ? line.get(3).toString(): null)
                    .codePlat((line.get(4) != null) ? line.get(4).toString(): null)
                    .labels((line.get(5) != null) ? line.get(5).toString(): null)
                    .allergenes((line.get(6) != null) ? line.get(6).toString(): null)
                    .build();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return restaurant;
    }
}
