package fr.bigdata.functions;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.function.Function;

import static org.apache.spark.sql.functions.count;

public class GroupByServiceFunc implements Function<Dataset<Row>, Dataset<Row>> {
    @Override
    public Dataset<Row> apply(Dataset<Row> restaurants) {
        return restaurants.groupBy("Service").agg(count("Composante").as("Total"));
    }
}
