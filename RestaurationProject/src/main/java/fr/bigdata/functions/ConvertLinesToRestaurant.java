package fr.bigdata.functions;

import fr.bigdata.bean.Restaurant;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import java.util.function.Function;

public class ConvertLinesToRestaurant implements Function<Dataset<Row>, Dataset<Restaurant>> {
    @Override
    public Dataset<Restaurant> apply(Dataset<Row> rows) {
        Dataset<Restaurant> restaurants = rows.map((MapFunction<Row, Restaurant>) new LineToRestaurant()::apply, Encoders.bean(Restaurant.class));
        return restaurants;
    }
}
