package fr.bigdata.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Restaurant implements Serializable {
    private Date date;
    private String service;
    private String composante;
    private String plat;
    private String codePlat;
    private String ordrePlat;
    private String labels;
    private String allergenes;
}
